**Summary**

This is my asteroid blaster project created using Unity 3D. The goal is very simple, destroy all the asteroids.

**Directions**

1. Open the project in Unity3d
2. Navigate to the "Scenes" folder and choose one of Level 1-3  
3. Play the scene

**Controls**

* Use WASD keys to move the ship
* Use the left mouse button to shoot missiles
* Press the number keys 1, 2, or 5 for special actions
* Move the mouse while pressing the right mouse button to look around