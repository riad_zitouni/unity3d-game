#pragma strict

private static var levelScript : Level;
private var spawnPoint : Transform;
public var missiles : Transform;

private var delay : float = 0.0;

function Start () {
	
	levelScript = GameObject.FindGameObjectWithTag("Level").GetComponent(Level);
	spawnPoint = transform.Find("Spawn Point");
	
	
}

function Update () {
	move();
	
	if(delay < 0)
		shoot();
	else
		delay -= 0.1;
	
	destroy();
}

function destroy(){
	
	 if(transform.position.x <= -levelScript.worldLimit || transform.position.x >= levelScript.worldLimit)
		Destroy(gameObject);
	
	if(transform.position.y <= -levelScript.worldLimit || transform.position.y >= levelScript.worldLimit)
		Destroy(gameObject);
		
	if(transform.position.z <= 0 || transform.position.z >= levelScript.worldLimit * 2)
		Destroy(gameObject); 
}

function shoot(){
	
	Instantiate(missiles, spawnPoint.transform.position, transform.rotation);
	delay = 5.0;
}

function move(){
	rigidbody.AddForce(transform.forward * 200);
}