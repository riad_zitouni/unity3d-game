#pragma strict

public var divisions : int = 2;

private var direction : Vector3;
private var directionMultiplyer : float;

private static var spaceCraft : GameObject;
private var levelScript : Level;


function Start () {
	setDirectionMultiplyer();
	spaceCraft = GameObject.FindWithTag("Space Craft");
	levelScript = GameObject.FindWithTag("Level").GetComponent(Level);
	
	setDirection();
}

function Update(){
	move();
	destroy();
	checkBorders();
}

function setDirectionMultiplyer(){
	directionMultiplyer = Random.Range(0.01, 0.05);
}

function setDirection(){
	
	var x : float = Random.Range(-1, 1);
	var y : float = Random.Range(-1, 1);
	var z : float = -1.0;
	direction = Vector3(x, y, z);
}


function move(){
	transform.position += direction * directionMultiplyer;
}

function destroy(){
	
	if(transform.localScale.x < 0.25)
		Destroy(gameObject);
}

function checkBorders(){
	if(transform.position.x <= -levelScript.worldLimit || transform.position.x >= levelScript.worldLimit)
		direction.x = -direction.x;
	
	if(transform.position.y <= -levelScript.worldLimit || transform.position.y >= levelScript.worldLimit)
		direction.y = -direction.y;
		
	if(transform.position.z <= 0 || transform.position.z >= levelScript.worldLimit * 2)
		direction.z = -direction.z;
		
}