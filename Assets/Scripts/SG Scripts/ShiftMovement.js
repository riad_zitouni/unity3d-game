#pragma strict

public var axisObject : Transform;
private var axis : Vector3;
private var thrusting : boolean = false;
private var decelerating : boolean = false;
private var controlsScript : Controls;

function Start () {
	controlsScript = transform.parent.transform.parent.GetComponent(Controls);
	
}

function Update () {
	
	if(Input.GetKey(KeyCode.W)){
		thrusting = true;
		decelerating = false;
	}
	else if(Input.GetKey(KeyCode.S)){
		decelerating = true;
		thrusting = false;
	}
	else{
		thrusting = false;
		decelerating = false;
	}
		
	
		
	var rotationValue : float = controlsScript.throttle / controlsScript.topSpeed;
		
	if(thrusting && rotationValue  < 1)
		transform.RotateAround(axisObject.position, transform.right, 10 * Time.deltaTime);
	else if(decelerating && rotationValue  > 0)
		transform.RotateAround(axisObject.position, transform.right, -10 * Time.deltaTime);
	
}