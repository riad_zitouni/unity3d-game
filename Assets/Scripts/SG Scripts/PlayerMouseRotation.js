#pragma strict

public var lookSensitivity : float = 2;
public var lookSmoothDamp : float = 0.1;
public var currentYRotation : float;
public var isLooking : boolean = false;

private var yRotationV : float;
private var xRotationV : float;
private var yRotation : float;
private var xRotation : float;
private var currentXRotation : float;
private var initialRotation : Quaternion;
private var controlsScript : Controls;

function Start () {
	initialRotation = transform.rotation;
	
	controlsScript = transform.parent.gameObject.GetComponent(Controls);
	
}

function Update () {

	
	//if(isViewFree){
	if(Input.GetMouseButton(1)){
		updateRotations();
		isLooking = true;
	}else
		isLooking = false;
			
}


function updateRotations(){

	yRotation += Input.GetAxis("Mouse X") * lookSensitivity;
	xRotation -= Input.GetAxis("Mouse Y") * lookSensitivity;
	
	
	xRotation = Mathf.Clamp(xRotation, -90, 90);
	
	currentXRotation = Mathf.SmoothDamp(currentXRotation, xRotation,
	xRotationV, lookSmoothDamp);
	
	currentYRotation = Mathf.SmoothDamp(currentYRotation, yRotation,
	yRotationV, lookSmoothDamp);
	

	transform.rotation = Quaternion.Euler(currentXRotation, currentYRotation,
	 transform.parent.transform.rotation.eulerAngles.x * -1.0);
}

function resetRotation(){
	
	currentXRotation = Mathf.SmoothDamp(currentXRotation, 0,
	xRotationV, lookSmoothDamp);
	
	currentYRotation = Mathf.SmoothDamp(currentYRotation, 0,
	yRotationV, lookSmoothDamp);
	
	
	transform.rotation = Quaternion.Euler(currentXRotation, currentYRotation,
	 0);
	 
	xRotation = initialRotation.x;
	yRotation = initialRotation.y;
	
}