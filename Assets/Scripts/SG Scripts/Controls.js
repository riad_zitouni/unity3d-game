#pragma strict


private var maxRightRotationVector : Vector3 ;
private var maxLeftRotationVector : Vector3;

private var playerMouseRotationScript : PlayerMouseRotation;

@HideInInspector
public var worldLimit : int = 50;

@HideInInspector
public var xSpeed : float = 0;
@HideInInspector
public var zSpeed : float = 0;
@HideInInspector
public var throttle : float = 0.0;
@HideInInspector
public var topSpeed : float = 50.0;
@HideInInspector
public var mouseXSensitivity : float;
@HideInInspector
public var mouseYSensitivity : float;  

public var throttleMultiplier : float = 0.25;
public var sidewaysxSpeed : int = 1000;






function Start () {
	
	playerMouseRotationScript = GetComponentInChildren(PlayerMouseRotation);
	mouseXSensitivity = 50.0;
	mouseYSensitivity = 50.0;
}

function Update(){
	
	calculateMouseSpeed();
}

function FixedUpdate () {
	
		
	if(playerMouseRotationScript.isLooking != true){
		steerCraft();
		rotateCraft();
	}
	
	
	if(Input.GetKey(KeyCode.W))
		accelerate();
	else if(Input.GetKey(KeyCode.S))
		decelerate();
		
	applyThrottle();
	
}

function calculateMouseSpeed(){
	zSpeed = (2 * ((Input.mousePosition.x - Screen.width / 2) / Screen.width));
	xSpeed = (2 * ((Input.mousePosition.y - Screen.height / 2) / Screen.height));
}

function steerCraft(){
	
	transform.position.x = Mathf.Clamp(transform.position.x, -50, 50);
	rigidbody.AddForce(transform.right * sidewaysxSpeed * Input.GetAxis("Horizontal"));
}

function rotateCraft(){

	
	//if(xSpeed < 1){
		transform.Rotate(-xSpeed, zSpeed, 0);
	//}
}


function accelerate(){
	
	if(throttle < topSpeed)
		throttle += throttleMultiplier;
	 
}

function applyThrottle(){
	transform.position.y = Mathf.Clamp(transform.position.y, -worldLimit, worldLimit);
	rigidbody.AddRelativeForce(Vector3.forward * throttle * 100); 
}

function decelerate(){
	
	if(throttle > 0)
		throttle -= throttleMultiplier;
}

