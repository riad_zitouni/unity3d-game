#pragma strict

public var numberOfAsteroids : int = 10;

public var asteroidObject : Transform;
public var explosiveAsteroidObject : Transform;
public var frostyAsteroidObject : Transform;

public var randomZMin : float = 10;
public var randomZMax : float = 100;

public var hasExplosiveAsteroids : boolean = false;

public var level : int ;


public var randX : float = 50;

public var worldLimit : int = 50;


function Start () {
	
	if(level == 1)
		initializeLevel1();
	else if(level == 2)
		initializeLevel2();
	else if(level == 3)
		initializeLevel3();
}


function Update () {
	
}

function generateZ(){
}

function initializeAsteroids(){
	
	var object : Transform;
	
	for(var i : int = 0; i < numberOfAsteroids; i++){
		
		if(level == 1){
			object = frostyAsteroidObject;
		}
		
		else if(level == 2){
		
			if(i < numberOfAsteroids / 3)
				object = asteroidObject;
			object = explosiveAsteroidObject;
		}
		
		else if(level == 3){
			
			if(i < numberOfAsteroids / 3)
				object = asteroidObject;
			else if(i > numberOfAsteroids / 3 && i < numberOfAsteroids / 2)
				object = explosiveAsteroidObject;
			else
				object = frostyAsteroidObject;
			
		}
	
		var x : int = Random.Range(-worldLimit, worldLimit);
		var y : int = Random.Range(-worldLimit, worldLimit);
		var z : int = Random.Range(0, worldLimit * 2);
		
		Instantiate(object, Vector3(x, y, z), Quaternion.identity);
		  
	}
}

function initializeLevel1(){

	for(var i : int = 0; i < numberOfAsteroids; i++){
	
	 	var x : int = Random.Range(-worldLimit, worldLimit);
		var y : int = Random.Range(-worldLimit, worldLimit);
		var z : int = Random.Range(0, worldLimit * 2);
		
		Instantiate(asteroidObject, Vector3(x, y, z), Quaternion.identity);
	}
}

function initializeLevel2(){
	
	var object : Transform;
	
	for(var i : int = 0; i < numberOfAsteroids; i++){
	
		if(i < numberOfAsteroids / 3){
			object = asteroidObject;
		}else
			object = explosiveAsteroidObject;
	
	 	var x : int = Random.Range(-worldLimit, worldLimit);
		var y : int = Random.Range(-worldLimit, worldLimit);
		var z : int = Random.Range(0, worldLimit * 2);
		
		Instantiate(object, Vector3(x, y, z), Quaternion.identity);
	}
}

function initializeLevel3(){
	
	var object : Transform;
	
	for(var i : int = 0; i < numberOfAsteroids; i++){
	
		if(i < numberOfAsteroids / 3)
			object = asteroidObject;
		else if(i > numberOfAsteroids / 3 && i < numberOfAsteroids / 2)
			object = explosiveAsteroidObject;
		else
			object = frostyAsteroidObject;
	
	 	var x : int = Random.Range(-worldLimit, worldLimit);
		var y : int = Random.Range(-worldLimit, worldLimit);
		var z : int = Random.Range(0, worldLimit * 2);
		
		Instantiate(object, Vector3(x, y, z), Quaternion.identity);
	}
}

