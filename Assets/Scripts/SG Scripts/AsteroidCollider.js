#pragma strict

public var explosionParticle : Transform;

function Start () {

}

function Update () {

}

function OnCollisionEnter(){
	print("IN");
	explode();
	
}

function explode(){

	var object :GameObject = transform.parent.gameObject;
	var direction = Random.Range(1,18);
	transform.parent.transform.localScale /= 2;
	
	for(var i = 0; i < transform.parent.GetComponent(Asteroid).divisions; i++){
		var asteroid : GameObject = Instantiate(transform.parent.gameObject, transform.position, transform.rotation);
		asteroid.GetComponent(Asteroid).setDirection();
		asteroid.GetComponent(Asteroid).setDirectionMultiplyer();
	}
	
	var particleTransform : Transform = Instantiate(explosionParticle, transform.position, Quaternion.identity);
	//StartCoroutine(destroyExplosionParticle(particleTransform.gameObject, 2));
	
	
}

function destroyExplosionParticle(object : GameObject, waitTime : float){
	yield WaitForSeconds(waitTime);
	Destroy(object);
	
}