#pragma strict

private var controlsScript : Controls;

function Start () {
	controlsScript = transform.parent.transform.parent.GetComponent(Controls);
	
}

function Update () {
	updateSteering(); 	
}

function updateSteering(){
	
	var zSpeed : float = controlsScript.zSpeed;
	var xSpeed : float = controlsScript.xSpeed;
	transform.rotation = Quaternion.Euler(transform.parent.transform.parent.rotation.eulerAngles.x,
	transform.parent.transform.parent.rotation.eulerAngles.y, transform.parent.transform.parent.rotation.eulerAngles.z + -zSpeed * 100);
	
	
}