#pragma strict

private var craftParametersScript : CraftParameters;
private var weapon : Weapon;
private var currentMissiles : Transform[];

public var missile : Transform;
public var radialMissiles : Transform;
public var missileTower : Transform;

public static var maxMissiles : int = 20;

@HideInInspector
public static var size : int = 0; 

function Start () {
	craftParametersScript = transform.parent.transform.parent.GetComponent(CraftParameters);
	currentMissiles = new Transform[maxMissiles];
	
}	

function Update () {
	
	if(Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)){
		shoot();
	}else if(Input.GetKeyDown(KeyCode.Alpha5)){
		shootCrazyMissiles();
	}
	else if(Input.GetKeyDown(KeyCode.Alpha1)){
		shootRadialMissiles();
	}else if(Input.GetKeyDown(KeyCode.Alpha2)){
		shootMissileTower();
	}
	

		
}

function shoot(){
	
	if(missile != null)
		Instantiate(missile, transform.position, transform.rotation);
	
}

function shootCrazyMissiles(){
	
	for(var i = 0; i < 20; i++){
		var rotation : Quaternion = transform.rotation;
		rotation.y *= i * 10;
		
		if(missile != null)
			Instantiate(missile, transform.position, rotation);
	}
}

function shootRadialMissiles(){
	
	var position : Transform = transform.Find("Radial Missiles Shooting Spawn");
	
	if(radialMissiles != null){
			
		if(position != null)
			Instantiate(radialMissiles, position.transform.position, transform.rotation);	
	}
}

function shootMissileTower(){
	
	var position : Transform = transform.Find("Missile Tower Spawn");
	
	if(missileTower != null){
			
		if(position != null)
			Instantiate(missileTower, position.transform.position, transform.rotation);	
	}
}
