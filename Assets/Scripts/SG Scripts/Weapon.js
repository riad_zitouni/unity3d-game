#pragma strict
// Splits rocks power times
public var power : int = 2;
public var thrust : float = 10;

public var shootingSpawnPoint : Transform;
private var shootScript : Shoot;
private static var levelScript : Level;

function Start () {
	
	if(shootingSpawnPoint != null)
		shootScript = shootingSpawnPoint.GetComponent(Shoot);
	
	levelScript = GameObject.FindGameObjectWithTag("Level").GetComponent(Level);
		
	
}

function FixedUpdate(){
	move();
	destroy();
}

function OnCollisionEnter(collision : Collision){
	
	if(collision.gameObject.tag=="Asteroid" ||collision.gameObject.tag=="Explosive Asteroid"){
		Destroy(gameObject);
		if(shootScript.size > 0)
			shootScript.size--;
	}
}

function move(){
	rigidbody.AddForce(transform.forward * thrust);
}

function destroy(){
	
	 if(transform.position.x <= -levelScript.worldLimit || transform.position.x >= levelScript.worldLimit)
		Destroy(gameObject);
	
	if(transform.position.y <= -levelScript.worldLimit || transform.position.y >= levelScript.worldLimit)
		Destroy(gameObject);
		
	if(transform.position.z <= 0 || transform.position.z >= levelScript.worldLimit * 2)
		Destroy(gameObject); 
}

